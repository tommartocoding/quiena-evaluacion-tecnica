let jumboTitle      =   document.querySelector('#jumbo-title');
let navbar          =   document.querySelector('#navbar');
let navbarUl        =   document.querySelector('#navbar-ul');
let navLoginBtn     =   document.querySelector('#nav-login-btn');
let content         =   document.querySelector('#content');
let modalBkg        =   document.querySelector('#modal-bkg');
let modalContainer  =   document.querySelector('#modal-container');
let loginBtn        =   document.querySelector('#btn-login');
let modalAlert      =   document.querySelector('#modal-alert');


navbar.addEventListener('click', ev => {
    ev.preventDefault();
    ev.stopPropagation();
    if(ev.target.nodeName == 'LI' || ev.target.nodeName == 'A'){
        switch (ev.target.dataset.page) {
            case 'page1':
                history.pushState({},'','/uno');
                fetch('/api/uno')
                    .then(resp => {
                        if(resp.ok == false){
                            return Promise.reject(resp.json());
                        }
                        if(resp.ok == true){
                            return resp.json();
                        }
                    })
                    .then(data => {
                        jumboTitle.innerHTML = `Esta es la <br> ${data.title}`;
                        render();
                    })
                    .catch(error => {
                        console.log(error);
                    })
                    render(content);
                break;
            case 'page2':
                history.pushState({},'','/dos');
                fetch('/api/dos')
                    .then(resp => {
                        if(resp.ok == false){
                            return Promise.reject(resp.json());
                        }
                        if(resp.ok == true){
                            return resp.json();
                        }
                    })
                    .then(data => {
                        jumboTitle.innerHTML = `Bienvenido a la <br> pagina 2`;
                        let html = `
                                    <div class="wrapper">
            
                                        <div class="table">
                                        
                                            <div class="row header">
                                                <div class="cell">
                                                Servicio
                                                </div>
                                                <div class="cell">
                                                Precio
                                                </div>
                                                <div class="cell">
                                                Rentabilidad (Anual)
                                                </div>
                                                <div class="cell">
                                                Riesgo
                                                </div>
                                            </div>`; 
                    data.forEach(servicio => {
                        html += `
                                    <div class="row">
                                        <div class="cell" data-title="servicio">
                                        ${servicio.servicio}
                                        </div>
                                        <div class="cell" data-title="precio">
                                        ${servicio.precio}
                                        </div>
                                        <div class="cell" data-title="rentabilidad">
                                        ${servicio.rentabilidad}
                                        </div>
                                        <div class="cell" data-title="Location">
                                        ${servicio.riesgo}
                                        </div>
                                    </div>
                                `;
                    });                  
                                            
                    html += `   </div>
                            </div>`;                                      
                                    
                        
                        render(content, html);
                    })
                    .catch(error => {
                        console.log(error);
                    })
                break;
                case 'page3':
                    history.pushState({},'','/tres');
                    fetch('/api/tres')
                        .then(resp => {
                            if(resp.ok == false){
                                return Promise.reject(resp.json());
                            }
                            if(resp.ok == true){
                                return resp.json();
                            }
                        })
                        .then(data => {
                            jumboTitle.innerHTML = `Bienvenido a la <br> ${data.title}`;
                            render();
                        })
                        .catch(error => {
                            console.log(error);
                        })
                        render(content);
                    break;

            case 'login':
                    modalContainer.classList.add('active')
                break;
            case 'logout':
                fetch('/api/logout')
                    .then(resp => {
                        if(resp.ok == false){
                            return Promise.reject(resp.json());
                        }
                        if(resp.ok == true){
                            return resp.json();
                        }
                    })
                    .then(data => {
                        console.log(data);
                        window.location.href = '/';
                    })
                    .catch(error => {
                        console.log(error);
                    })
                    
                break;
            case 'user':
                history.pushState({},'','/user');
                fetch('/api/user')
                    .then(resp => {
                        if(resp.ok == false){
                            return Promise.reject(resp.json());
                        }
                        if(resp.ok == true){
                            return resp.json();
                        }
                    })
                    .then(user => {
                        jumboTitle.innerHTML = `Hola ${user.name} <br> es bueno tenerte devuelta :)`;
                        html = `
                                <div class="container">
                                    <h2>Estos son tus datos:</h2>
                                    <p>Email: ${user.email}</p>
                                </div>`;
                        render(content, html)
                    })
                    .catch(error => {
                        console.log(error);
                    })
                break;
            case 'admin':
                history.pushState({},'','/admin');
                fetch('/api/admin')
                    .then(resp => {
                        if(resp.ok == false){
                            return Promise.reject(resp.json());
                        }
                        if(resp.ok == true){
                            return resp.json();
                        }
                    })
                    .then(admin => {
                        jumboTitle.innerHTML = `Hola ${admin.name} <br> es bueno tenerte devuelta :)`;
                        let html = `
                                            <div class="container">
                                                <h2>Su tipo de rol es: ${admin.roles}</h2>
                                            </div>`;
                        render(content, html)
                    })
                    .catch(error => {
                        console.log(error);
                    })
                break
        }
    }
});

loginBtn.addEventListener('click', ev => {
    ev.stopPropagation;
    ev.preventDefault();
    let loginForm       =   document.querySelector('#login-form');
    let form = new FormData(loginForm);
    let json = formDataToJson(form);
    let reqConfigs  =   {
        "credentials":"include", 
        "method":"POST",
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
        "body": json
    }  

    fetch('/api/login', reqConfigs)
        .then(resp => {
            if(resp.ok == false){
                if(resp.status == 401){
                    let html = '<p>Error de Autentificacion</p>';
                    modalAlert.classList.add('warning');
                    modalAlert.classList.add('active');
                    render(modalAlert, html)
                }
                return Promise.reject(resp.json());
            }
            if(resp.ok == true){
                console.log(resp);
                return resp.json();
            }
        })
        .then(user => {
            modalContainer.classList.remove('active');
            if(user.roles.includes('ROLE_USER')){
                navbarUl.removeChild(navLoginBtn);
                navbarUl.innerHTML += `<li data-page="user">User</li><li data-page="logout">Logout</li>`;
            }
            if(user.roles.includes('ROLE_ADMIN')){
                navbarUl.removeChild(navLoginBtn);
                navbarUl.innerHTML +=  `<li data-page="user">User</li>
                                        <li data-page="admin">Admin</li>
                                        <li data-page="logout">Logout</li>`;
            }
        })
        .catch(error => {
            console.log(error);
        })

})

modalBkg.addEventListener('click', ev => {
    modalContainer.classList.remove('active');
    modalAlert.classList.remove('active');
}, false)

window.addEventListener('locationchange', ev => {
	console.log(ev);
});

function formDataToJson(formData){
    let obj = Object.fromEntries(formData.entries());
    let json = JSON.stringify(obj);
    return json;
}

function render(container, html = ''){
        container.innerHTML = html;
}


















// btnUser.addEventListener('click', ev => {
//     ev.preventDefault();
//     ev.stopPropagation();
//     fetch('/user')
//         .then(resp => {
//             if(resp.ok == false){
//                 return Promise.reject(resp.json());
//             }
//             if(resp.ok == true){
//                 return resp.json();
//             }
//         })
//         .then(data => {
//             console.log(data);
//         })
//         .catch(error => {
//             console.log(error);
//         })

// })

// btnLogin.addEventListener('click', ev => {
//     ev.preventDefault();
//     ev.stopPropagation();
//     // let fd = new FormData(form);
//     // let jsonData = '{';
//     // for(var pair of fd.entries()) {
//     //     data = pair[0].split(pair)
//     //     jsonData += `"${data[0]}" : ${data[1]},`
//     // }
//     // jsonData += '}';

//     let reqConfigs  =   {
//         "credentials":"include", 
//         "method":"POST",
//          headers: {
//              'Content-type': 'application/json; charset=UTF-8'
//          },
//         "body": '{"username":"tom", "password":"1234"}'
//     }  

//     fetch('/login', reqConfigs)
//         .then(resp => {
//             if(resp.ok == false){
//                 return Promise.reject(resp.json());
//             }
//             if(resp.ok == true){
//                 return resp.json();
//             }
//         })
//         .then(data => {
//             console.log(data);
//         })
//         .catch(error => {
//             console.log(error);
//         })
// })

// btnLogout.addEventListener('click', ev => {
//     ev.preventDefault();
//     ev.stopPropagation();
//     fetch('/logout')
//         .then(resp => {
//             if(resp.ok == false){
//                 return Promise.reject(resp.json());
//             }
//             if(resp.ok == true){
//                 return resp.json();
//             }
//         })
//         .then(data => {
//             console.log(data);
//         })
//         .catch(error => {
//             console.log(error);
//         })
// })
