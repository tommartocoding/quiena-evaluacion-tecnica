<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Servicios;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/api/uno", name="page1", methods={"GET"})
     */
    public function pageOne()
    {
        return $this->json(['title'  => 'pagina 1']);
    }
    
    /**
     * @Route("api/dos", name="page2", methods={"GET"})
     */
    public function pageTwo(SerializerInterface $serializer)
    {   
        $dManager   =   $this->getDoctrine()->getManager();
        $servicios  =   $dManager->getRepository('AppBundle:Servicios')->findAll();
        $data       =   $serializer->serialize($servicios, 'json');
        // return $this->json($data);
        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @Route("/api/tres", name="page3", methods={"GET"})
     */
    public function pageThree()
    {
        return $this->json(['title'  => 'pagina 3']);
    }

    /**
     * @Route("/api/user", name="user" , methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function user()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        return $this->json([
            'name'  =>  $user->getNombre(),
            'email'  => $user->getEmail(),
        ]);
    }

    /**
     * @Route("/api/admin", name="admin", methods={"GET"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function admin(Request $request)
    {
        $user = $this->getUser();
        return $this->json([
            'name'  => $user->getNombre(),
            'roles'     => $user->getRoles(),
        ]);
    }

    /**
     * @Route("/api/login", name="login", methods={"POST"})
     */
    public function login()
    {
            $user = $this->getUser();
            return $this->json([
                'username'  => $user->getUsername(),
                'roles'     => $user->getRoles(),
            ]);
    }

    /**
     * @Route("/api/logout", name="logout", methods={"GET"})
     */
    public function logout()
    {
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAns()
    {
        return $this->json([
            'status'    => 'success',
            'message'   => 'Sesion cerrada correctamente',
        ]);
    }

    /**
    * @Route("/{anything}", name="404", requirements={"anything"=".*"})
    */
    public function fail(){
        return $this->render('404.html.twig',[], new Response('',404));
    }
}
