Examen Tecnico Quiena
========================

1_ Descargar el Proyecto.
2_ Cargar datos de BBDD y Mailing. 
3_ Ejecutar Composer Install.


Respuestas Teoricas:
--------------
1) 
  - Mantener bien documentado el codigo ya que ayuda a la comprension futura del mismo y la colaboracion con otros programadores.
  - Tener un standar de nomeclaturas, sobre todo cuando se colabora con otros programadores ya que si cada uno sigue el suyo mantener el codigo y  
  colaborar puede ser un caos.
  - Usar nombres de variables comprensibles y con sentido.

2)
  - Ayuda a trabajar en equipo ya que aporta un marco de trabajo.
  - Brinda un monton de soluciones que se usan de manera frecuente sin tener que desarrollarlas de cero cada vez que se quiere iniciar un proyecto.
  - Ayudan a estructurar los archivos de un proyecto.
  - Facilitan la implementacion de patrones de diseño, en el caso de laravel y symfony mvc.
  - Ayudan a hacer el codigo mantenible.


4)
  - Son metodos reservados que tienen funcionalidades ya asignadas dentro de las clases de php. Y no deben redefinirse salvo que se quiera modificar su funcionalidad.

5)
  - Primero observaria si hay algun log que este expresando informacion relevante para la resolucion del error.
  - Luego revisaria el funcionamiento del servidor, el espacio de memoria, temperatura, rendimiento y de ser necesario lo reiniciaria.

